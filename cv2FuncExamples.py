# pip install ultralyntics
import pytz
from datetime import datetime
import time
import cv2
import os
import torch
from ultralytics import YOLO

def load_model(path= "yolov8m.pt"):
    # Load the YOLOv8 model
    model = YOLO(path)
    return model

def create_dirs(path=""):
    # if directory is not present
    try:
        if not os.path.isdir(path):
            os.makedirs(path)
    except:
        print("Error in creating directory")

time = datetime.now(pytz.timezone('Asia/Kolkata'))

def video(input_path, output_path=""):
    # Open the video file
    cap  = cv2.VideoCapture(input_path)
    if not cap.isOpened():
        raise IOError("Couldn't open webcam or video")
    video_FourCC = cv2.VideoWriter_fourcc(*'MP4V')# int(cap.get(cv2.CAP_PROP_FOURCC)) # cv2.VideoWriter_fourcc(*"mp4v")
    video_fps = cap.get(cv2.CAP_PROP_FPS)
    video_size = (
        int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
        int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)),
    )
    
    # makedirs
    create_dirs(output_path)
        
    isOutput = True if output_path != "" else False
    if isOutput:
        print(
            "Processing {} with frame size {} at {:.1f} FPS".format(
                os.path.basename(input_path), video_size, video_fps
            )
        )
        # print("!!! TYPE:", type(output_path), type(video_FourCC), type(video_fps), type(video_size))
        date_string = time.strftime("%Y-%m-%d-%H-%M-%S")
        output_path = output_path + '/video-' + date_string + '.mp4'
        out = cv2.VideoWriter(output_path, video_FourCC, video_fps, video_size)

        # Loading model
        model = load_model("model/yolov8m.pt")
        
    # Loop through the video frames
    while cap.isOpened():
        # Read a frame from the video
        success, frame = cap.read()

        if success:
            # Run YOLOv8 inference on the frame
            results = model(frame, device='mps')

            # Visualize the results on the frame
            annotated_frame = results[0].plot()
            
            if isOutput:
                out.write(annotated_frame)
            
            # # # Display the annotated frame
            # cv2.imshow("YOLOv8 Inference", annotated_frame)

            # # Break the loop if 'q' is pressed
            # if cv2.waitKey(1) & 0xFF == ord("q"):
            #     break
        else:
            # Break the loop if the end of the video is reached
            break
        success, output_path, error_description = True, output_path, "None"
    return success, output_path, error_description

    # # Release the video capture object and close the display window
    # cap.release()
    # cv2.destroyAllWindows()

# video("test/weapon-test.mp4", "media/detect/video")

def image(img, output_path):
    # reading image
    image = cv2.imread(img)
    
    # Loading model
    model = load_model(path="model/yolov8m.pt")
    
    # Run YOLOv8 inference on the frame
    results = model(image, device='mps')

    # Visualize the results on the frame
    annotated_image = results[0].plot()
        
    # creating folders if not exist
    create_dirs(path=output_path)
    isOutput = True if output_path != "" else False
    
    if not annotated_image.any(): 
        print("String is empty not detection")
        return None
    else:
        date_string = time.strftime("%Y-%m-%d-%H-%M-%S")

        imgpath = output_path +'/img-'+ date_string +'.png'
        
        if isOutput:
            cv2.imwrite(imgpath, annotated_image)
        # print(out_pred)
        success, output_path, error_description = True, imgpath, "None"
        return success, output_path, error_description

image("test/test.jpg", "media/detect/image")
